#! /usr/local/bin/python3.6

import sys
import logging
logging.basicConfig(filename='/var/log/lispy.wsgi.log',level=logging.DEBUG)

sys.path.append('/usr/local/www/apache24/data/lg.lisp-views.org/')
sys.path.append('/usr/local/lib/python3.6/site-packages/') 

from lg import app as application
