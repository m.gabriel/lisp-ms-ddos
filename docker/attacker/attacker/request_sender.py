#! /usr/bin/python3.6
import sys
import time
import socket

from lispy.utils import *
from lispy.lisp import MapRequestMessage, EncapsulatedControlMessage, MapReplyMessage
from lispy.ip import IPv4Packet, IPv6Packet, UDPMessage
from ipaddress import IPv4Address

my_ip = "109.132.88.189"
map_resolver = "173.36.254.164"  # Cisco

def do_map_request(map_resolver, dst_eid, nonce0):
    ''' 
        Perform the map_request to the given map resolver for the given EID with the given nonce
        The function return a MapReplyMessage Object, the address that replied and the RTT
    '''
    # Randomly choose a source port number
    # port_source = random.choice(range(MIN_EPHEMERAL_PORT, 65535))
    port_source = 4342

    # Resolve EID and MR IP from name
    mr_addr = socket.getaddrinfo(map_resolver, LISP_CONTROL_PORT, 0, 0, socket.SOL_UDP)
    eid_addr = socket.getaddrinfo(dst_eid, 0, 0, 0, socket.SOL_UDP)
   
    # IP string TO IPV4/6Address object
    # ip_map_resolve = ipaddress.ip_address(str(mr_addr[0][4][0]));
    # print("MR IP : " + str(ip_map_resolve))
    # ip_eid = ipaddress.ip_address(str(eid_addr[0][4][0]));
    # print("EID IP : " + str(ip_eid))

    ip_map_resolve = ipaddress.ip_address(map_resolver)
    # print("MR IP : " + str(ip_map_resolve))
    ip_eid = ipaddress.ip_address(dst_eid)
    # print("EID IP : " + str(ip_eid))

    # Get our IP Address, our address must be routable on the internet
    # ip_my = ipaddress.ip_address(get_my_ipaddress());
    ip_my = ipaddress.ip_address(my_ip);
    # print("MY IP : " + str(ip_my))
    ip_my6 = ipaddress.ip_address(get_my_ipaddress6());
    # print("MY IPv6 : " + str(ip_my6))

    # Now we build this:
    '''

            0                   1                   2                   3
            0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
           +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
         / |                       IPv4 or IPv6 Header                     |
       OH  |                      (uses RLOC addresses)                    |
         \ |                                                               |
           +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
         / |       Source Port = xxxx      |       Dest Port = 4342        |
       UDP +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
         \ |           UDP Length          |        UDP Checksum           |
           +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       LH  |Type=8 |S|                  Reserved                           |
           +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
         / |                       IPv4 or IPv6 Header                     |
       IH  |                  (uses RLOC or EID addresses)                 |
         \ |                                                               |
           +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
         / |       Source Port = xxxx      |       Dest Port = yyyy        |
       UDP +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
         \ |           UDP Length          |        UDP Checksum           |
           +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       lcm |                      LISP Control Message                     |
           +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
           
    '''
    
    # Building the LISP Control Message (lcm)
    lcm = MapRequestMessage(eid_prefix=ip_eid, itr_rloc=ip_my, nonce=nonce0)

    # Building UDP Header
    udp = UDPMessage(source_port=port_source, destination_port=LISP_CONTROL_PORT, payload=lcm)
    # Building Inner IP Header
    # Computing udp checksum, checksum is only mandatory in IPV6
    if isinstance(ip_eid, IPv4Address):
        ih = IPv4Packet(source=ip_my, destination=ip_eid, payload=udp, protocol=IPPROTO_UDP)
        udp.checksum = udp.calculate_checksum(source=ip_my, destination=ip_eid)
    else:
        ih = IPv6Packet(source=ip_my6, destination=ip_eid, payload=udp, next_header=IPPROTO_UDP)
        udp.checksum = udp.calculate_checksum(source=ip_my6, destination=ip_eid)

    # Building LISP Header
    lh= EncapsulatedControlMessage(payload=ih)
   
    #UDP AND the Outer IP Header (OH) are built by the kernel
    
    # Creating the socket
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    # set a 3 sec timeout
    sock.settimeout(3)
    # sock.bind((str(ip_my),port_source))
    sock.bind(("0.0.0.0",port_source))


    # send the request and wait for the reply
    # before = time.time();
    sock.sendto(lh.to_bytes(), mr_addr[0][4])
    print("Map-Request sent to " + str(mr_addr[0][4]))

    # Receive map-reply data
    try:
    data, addr = sock.recvfrom(512)
    print("Map-Reply received")

    # print(data)
    # after = time.time();
   
    # Build a MapReplyMessage Object from the received bytes and return it
    # Also return the ip address that replied to us and the RTT
    # MyMapReply = MapReplyMessage.from_bytes(data)

    # return MyMapReply, addr, (after-before)

    return
   

if __name__ == "__main__":
    print("Hello world!")

    eids = []

    # Get the different eids to test (one for each address space)
    with open("data/mappings.txt", 'r') as data:
        for line in data.readlines():
            eid = line.split('/')[0]
            eids.append(eid)

    # Send the map requests
    for eid in eids:
        # dst_eid = input("EID: ")
        print("EID: " + eid)
        do_map_request(map_resolver, eid, get_a_nonce());

        time.sleep(0.2)
