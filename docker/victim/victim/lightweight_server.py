#! /usr/bin/python3.6
import socket

"""
This server maps the 4342 port used by LISP in order to assess the reception of
the different Map-Reply packets during the attack.
"""

port = 4342
bufsize = 1024

if __name__ == "__main__":
    print("Listening on port 4342")

    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.bind(("0.0.0.0", port))

    while True:
        bap = sock.recvfrom(bufsize)

        msg = bap[0]
        addr = bap[1]

        print("Receiving msg of size {} from {}".format(len(msg), addr))
