#!/bin/bash
# Compute the amount of data received on interface eth0 every second

bytes=$(cat /sys/class/net/eth0/statistics/rx_bytes)

while sleep 1
do
	old=$bytes
	bytes=$(cat /sys/class/net/eth0/statistics/rx_bytes)

	echo $bytes $old | awk '{ printf("%f\n", $1-$2) }'
done
