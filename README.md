# Denial-of-Service attack using the LISP mapping system

This repository contains the different scripts and GNS3 configuration files in
order to reproduce a testbed on which to perform a denial-of-service
attack by amplification using a LISP-DDT mapping system.

## Prerequisite

This project requires a system on which **GNS3**, **dynamips** and **Docker**
are installed.

![screenshot](gns3_topology.png)

## Nodes

- [attacker](docker/attacker/)
- [victim](docker/victim/)
- [PC1](gns3/project-files/vpcs/c99ab92e-9f6e-4832-ac2d-b1494d12f72b/startup.vpc)

## Routers

Here is a list of the configuration of the different Cisco routers composing the
virtual network.

### LISP-DDT

- [ddt-root](gns3/project-files/dynamips/d2d68bb8-84a1-4732-bb82-699447e7ff6b/configs/i1_startup-config.cfg)
- [ddt-tld1](gns3/project-files/dynamips/7d45aa80-1e9c-4ae8-9236-da202b005af6/configs/i2_startup-config.cfg)
- [ddt-tld2](gns3/project-files/dynamips/b7777c10-34f2-460b-b074-8ecb9ab1d698/configs/i3_startup-config.cfg)

### Map-Resolver / Map-Server

- [eqx-mrms](gns3/project-files/dynamips/a10a570c-e1cd-4fb0-a4ae-11bf06733665/configs/i6_startup-config.cfg)
- [cisco-mrms](gns3/project-files/dynamips/9d112951-fc47-4914-acd5-cd12f2c57f7b/configs/i5_startup-config.cfg)
- [intouch-mrms](gns3/project-files/dynamips/2545eacd-65c5-4526-8790-6962190569f6/configs/i6_startup-config.cfg)
- [upc-mrms](gns3/project-files/dynamips/9202b803-920f-4ec2-9f78-30ed32130952/configs/i7_startup-config.cfg)
- [iij-mrms](gns3/project-files/dynamips/e077524a-1615-481d-a5ac-70013ad24194/configs/i8_startup-config.cfg)
- [sg-mrms](gns3/project-files/dynamips/fe9d8534-5b7c-4097-974f-e4d92f3d7d9f/configs/i9_startup-config.cfg)

### LISP Site

- [us-site1-xtr](gns3/project-files/dynamips/dccc9bcd-73a7-4f81-a624-86c565c46bfa/configs/i5_startup-config.cfg)

### Core Network

- [DFZ](gns3/project-files/dynamips/0460b418-3f90-468c-91a9-458f87d9dc25/configs/i2_startup-config.cfg)
- [US](gns3/project-files/dynamips/bf85a91d-9a9a-4ec4-9867-be2fe518e9c3/configs/i1_startup-config.cfg)
- [EU](gns3/project-files/dynamips/ef2ec855-7123-4ff0-86ee-6e88e8f70827/configs/i3_startup-config.cfg)
- [Asia](gns3/project-files/dynamips/f34f4775-45e7-470d-bd34-74a6645c1144/configs/i4_startup-config.cfg)

## About

This project was developed as part of my Master Thesis at University of Liège.
