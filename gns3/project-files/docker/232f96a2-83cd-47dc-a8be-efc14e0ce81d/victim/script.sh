#!/bin/bash

# while sleep 1 ; do ./script.sh ; done

bytes=$(cat /sys/class/net/eth0/statistics/rx_bytes)
timestamp=$(date +%s)

echo "$timestamp;$bytes" >> stats.txt
