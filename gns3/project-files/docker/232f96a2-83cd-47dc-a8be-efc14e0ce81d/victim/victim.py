#! /usr/bin/python3.6
import socket

port = 4342
bufsize = 1024

if __name__ == "__main__":
    print("Listening on port 4342")

    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

    sock.bind(("0.0.0.0", port))

    while True:
        bap = sock.recvfrom(bufsize)

        msg = bap[0]
        addr = bap[1]

        print("Receiving msg of size {} from {}".format(len(msg), addr))

        # print(msg)
        # print(addr)
